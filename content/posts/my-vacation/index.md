---
title: "My Vacation"
date: 2021-04-24T16:00:13-05:00
draft: false
author: Alejandro Cerro
year: "2021"
month: "2021/04"
categories:
- Personal
tags:
- Family
---
Some picture from South Dakota

<!--more-->

{{< postimage "images/rushmore.jpg" "Mount Rushmore" >}}
{{< postimage "images/bison.jpg" "Bison" >}}
{{< postimage "images/badlands.jpg" "BADlnads" >}}

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat.