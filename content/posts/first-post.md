---
title: "First Post"
banner: "images/turntable.jpg"
banner_alt: "turntable"
date: 2021-04-12T22:12:14-05:00
draft: false
author: Alejandro Cerro
year: "2021"
month: "2021/04"
categories:
- Personal
- Thoughts 
tags:
- software
- html
disableComments: true
---

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua.

<!--more-->

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Phasellus nec sem in justo pellentesque facilisis. Suspendisse eu ligula. Cras sagittis. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Maecenas vestibulum mollis diam.

Pellentesque ut neque. Ut varius tincidunt libero. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Fusce egestas elit eget lorem.