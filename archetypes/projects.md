---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
image: //picsum.photos/640/150
alt_text: "{{ replace .Name "-" " " | title }} screenshot"
summary: "Summary of the {{ replace .Name "-" " " | title }} project"
tech_used:
- JavaScript
- CSS
- HTML
---

Description of the {{ replace .Name "-" " " | title }} project ...